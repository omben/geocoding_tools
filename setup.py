from setuptools import find_packages, setup

import sys

install_requires = ['fuzzywuzzy',
                    'geopandas>=0.6.1',
                    'pyproj',
                    'shapely',
                    "pandas>=1.0.3",
                    "folium",
                    'contextily']
if 'win' not in sys.platform :
    install_requires.append('python-Levenshtein')

setup(
    name='geocoding_tools',
    version='0.2.2',
    url='https://gitlab.com/gorenove/geocoding_tools',
    packages=find_packages(),
    install_requires=install_requires,
    package_data = {'': ['*.txt','*.csv','*.zip'],
                                        'geocoding_tools':['tests/sample_data/*.csv']
                                     },
)


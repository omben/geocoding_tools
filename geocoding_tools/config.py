from multiprocessing import cpu_count
import os

addok_ban_urls = {
    'ADDOK_URL': os.environ.get('ADDOK_URL', 'https://api-adresse.data.gouv.fr'),
    'GEO_API_URL': os.environ.get('GEO_API_URL',
                                  'https://geo.api.gouv.fr'),
    'BAN_ADDR_DEPT_URL': os.environ.get('BAN_ADDR_DEPT_URL',
                                        'https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/adresses-{dept}.csv.gz')
}
addok_ban_urls.update({
    'ADDOK_SEARCH_CSV_URL': addok_ban_urls['ADDOK_URL'] + '/search/csv',
    'ADDOK_SEARCH_URL': addok_ban_urls['ADDOK_URL'] + "/search/?q=",
    'COMMUNES_DEPT_URL': addok_ban_urls['GEO_API_URL'] + "/departements/{code}/communes?format=geojson",
}
)
multiprocessing_config = {'processes': cpu_count() - 2,
                          'sleep_time': 0.01,
                          "nb_async_jobs": cpu_count() - 2}

exception_config ={'skip_failed_geocoding':False}

debug_config ={'debug_rejected':False}
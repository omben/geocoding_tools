import pandas as pd
import re
import unicodedata
import numpy as np
from .assets.dictionaries import abv_words, abv_words_most_used, titres_communs

def strip_accents_and_rep_spec_char(s):
    """
    remove accents from a string.
    Parameters
    ----------
    s :str
    string

    Returns
    -------

    """
    s = s.replace('µ', 'u')
    s = s.replace('ù', 'u')
    s = s.replace('@', 'a')
    s = s.replace('ç', 'c')

    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


def remove_repeated_consecutive_word(s):
    """
    remove duplicate consecutive words :  ex :  1 1 bis rue jean mermoz -> 1 bis rue jean mermoz
    Parameters
    ----------
    s : string


    Returns
    -------

    """

    last_word = ''
    s_filt = list()
    for word in s.split(' '):
        if word != last_word:
            s_filt.append(word)
        last_word = word
    x = ' '.join(s_filt)

    return x


def build_concat_addr_from_table(df, addr_cols):
    def format_addr_col_for_concat(x):
        if isinstance(x, float):
            return ''
        elif x.strip() != '' and x.strip() != 'nan':
            x = ' '.join(x.strip().split())
            return x + ' '
        elif x.strip() == 'nan':
            return ''
        else:
            return x

    df = df.copy()
    df[addr_cols] = df[addr_cols].fillna('')
    s_addr = pd.Series(index=df.index).fillna('')
    for col in addr_cols:
        s_addr += df[col].apply(lambda x: format_addr_col_for_concat(x))
    s_addr = clean_addr_serie(s_addr)
    s_addr = s_addr.str.title().str.strip()
    return s_addr


def clean_citycode_serie(s):
    '''
    fixing citycode and postcode fields
    Parameters
    ----------
    s :pd.Series

    Returns
    -------
    s : pd.series
    '''
    null = s.isnull()
    s = s.astype(str)
    s = s.str.replace('\.0', '',regex=True).replace('nan', '')
    s = s.str.zfill(5)
    s.loc[null] = np.nan
    return s


def clean_addr_serie(s):
    s = s.fillna('')
    s = s.replace('null', '')
    s = s.replace('nan', '')
    s = s.apply(lambda x: clean_addr_string(x))
    return s


def clean_addr_string(x, abv_words=abv_words):

    x = normalize_better(x,abv_words=abv_words)
    x = remove_bracked_words(x)

    return x


bad_chars_to_space = ['\\n', '\\t', '\\r', '_']
bad_addr_words = [
    "appartement ",
    "apt ",
    "appt ",
    "apt.",
    "non communiquee",
    "non communique",
    "nc ",
    "n°"]


def normalize_better(addr,abv_words=abv_words_most_used):
    addr = strip_accents_and_rep_spec_char(addr)
    addr = unicodedata.normalize("NFD", addr.lower()) \
        .encode('ascii', 'ignore') \
        .decode("utf-8")
    for bad_char in bad_chars_to_space:
        addr = addr.replace(bad_char, ' ')
    for bad_word in bad_addr_words:
        addr = addr.replace(bad_word, " ")
    addr = re.sub('\W+', ' ', addr)
    addr = re.sub(' +', ' ', addr)
    if any(el in addr for el in abv_words):
        for k, v in abv_words.items():
            addr = addr.replace(k, v)
    addr = remove_repeated_consecutive_word(addr)
    addr = addr.title().strip()
    return addr


def remove_bracked_words(addr):
    bracketed_words = re.compile('\(([^)]*)\)')
    addr = re.sub(bracketed_words, '', addr)
    return addr


def remove_title_proper_noun(addr):
    for title in titres_communs:
        addr = addr.replace(title, '')
    return addr


def clean_address_for_compare(addr):
    addr = normalize_better(addr)
    addr = remove_bracked_words(addr)
    addr = remove_title_proper_noun(addr)
    return addr

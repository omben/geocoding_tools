import pandas as pd

def main():
    pass

def read_and_cleanup_enedis_addr_table(file_path):

    table = pd.read_csv(file_path, sep=';', encoding='ansi')
    table['code_insee'] = table.CODE_IRIS.apply(lambda x: x[0:5])

    return table
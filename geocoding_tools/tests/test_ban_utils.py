from geocoding_tools.ban_utils import get_communes_table_by_dept


def test_get_communes_table_by_dept():

    com_ban_geo = get_communes_table_by_dept(['75', '92', '93', '94'])

    com_ban_geo.plot(figsize=(17, 17), column='codeDepartement')


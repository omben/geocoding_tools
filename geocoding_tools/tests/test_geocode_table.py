from pkg_resources import resource_filename
import pandas as pd
from geocoding_tools.geocode_table import simple_geocode_table, advanced_geocode_table
from geocoding_tools.scripts_specific_bases.dpe import cleanup_dpe_table
from geocoding_tools.config import addok_ban_urls
from geocoding_tools.exceptions import AddokFullFailGeocodeException
import pytest

def test_geocode_simple_enedis():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')
    table['code_insee'] = table.CODE_IRIS.apply(lambda x: x[0:5])  # code insee récupéré de l'iris.
    addr_cols_ordered = ['ADRESSE']  # pour les données enedis un seul champs adresse est utilisé(obligatoire)
    cityname_col = 'NOM_COMMUNE'  # le champs du nom de la commune(optionnel : fortement recommandé)
    citycode_col = 'code_insee'  # le champs du code insee(recommandé)
    postcode_col = None  # pas de code postal dans la base enedis(recommandé)
    keep_debug_cols = True  # garde les colonnes intermédiaire de calcul du géocodage (a desactiver pour optimiser mémoire)
    id_col = None  # colonne d'identifiant utilisé dans l'échantillon (sinon par défaut génère un id qui est le numéro de ligne en format str)
    addok_search_csv_kwargs = None  # keyword arguments passés à l'appel à addok_search_csv(geocoding_tools.addok_utils.run_get_addok_search_csv_by_chunks)
    dept_col = None  # colonne renseignant le département
    table_geo_simple = simple_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                            citycode_col=citycode_col,
                                            postcode_col=postcode_col, id_col=id_col,
                                            keep_debug_cols=keep_debug_cols,
                                            )
    assert (table_geo_simple.shape[0] == 1000)


def test_geocode_advanced_enedis():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')
    table['code_insee'] = table.CODE_IRIS.apply(lambda x: x[0:5])
    addr_cols_ordered = ['ADRESSE']  # pour les données enedis un seul champs adresse est utilisé(obligatoire)
    cityname_col = 'NOM_COMMUNE'  # le champs du nom de la commune(optionnel : fortement recommandé)
    citycode_col = 'code_insee'  # le champs du code insee(recommandé)
    postcode_col = None  # pas de code postal dans la base enedis(recommandé)
    keep_debug_cols = True  # garde les colonnes intermédiaire de calcul du géocodage (a desactiver pour optimiser mémoire)
    id_col = None  # colonne d'identifiant utilisé dans l'échantillon (sinon par défaut génère un id qui est le numéro de ligne en format str)
    simple_geocoding_score_threshold = 0.8
    addok_search_csv_kwargs = None  # keyword arguments passés à l'appel à addok_search_csv(geocoding_tools.addok_utils.run_get_addok_search_csv_by_chunks)
    dept_col = None  # colonne renseignant le département
    res, comb = advanced_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                            citycode_col=citycode_col,
                                            postcode_col=postcode_col, id_col=id_col,
                                            simple_geocoding_score_threshold=simple_geocoding_score_threshold,
                                            keep_debug_cols=keep_debug_cols)

    assert (res.shape[0] == 1000)


def test_geocode_simple_dpe_38():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_td001_dpe_38.csv')

    table = pd.read_csv(file_path, dtype=str, index_col=0)
    table = cleanup_dpe_table(table)

    addr_cols_ordered = ['numero_rue', 'type_voie', 'nom_rue']
    cityname_col = 'commune'
    citycode_col = 'code_insee'
    postcode_col = 'code_postal'
    keep_debug_cols = True
    id_col = 'numero_dpe'
    addok_search_csv_kwargs = None
    dept_col = None
    simple_geocoding_score_threshold = 0.8

    res = simple_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                               citycode_col=citycode_col,
                               postcode_col=postcode_col, keep_debug_cols=True, id_col=id_col)

    assert (res.shape[0] == 1000)


def test_geocode_advanced_dpe_38():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_td001_dpe_38.csv')

    table = pd.read_csv(file_path, dtype=str, index_col=0)
    table = cleanup_dpe_table(table)

    addr_cols_ordered = ['numero_rue', 'type_voie', 'nom_rue']
    cityname_col = 'commune'
    citycode_col = 'code_insee'
    postcode_col = 'code_postal'
    keep_debug_cols = True
    id_col = 'numero_dpe'
    addok_search_csv_kwargs = None
    dept_col = None
    simple_geocoding_score_threshold = 0.8

    res, comb = advanced_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                       citycode_col=citycode_col,
                                       postcode_col=postcode_col, keep_debug_cols=True, id_col=id_col,
                                       simple_geocoding_score_threshold=simple_geocoding_score_threshold)

    assert (res.shape[0] == 1000)
    assert (res.query('numero_dpe=="1338L1000113E"')['address_concat'].iloc[
                0] == '86 Avenue Jean Perrot 38100 Grenoble')

def test_geocode_advanced_dpe_38_nopostcode():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_td001_dpe_38.csv')

    table = pd.read_csv(file_path, dtype=str, index_col=0)
    table = cleanup_dpe_table(table)

    addr_cols_ordered = ['numero_rue', 'type_voie', 'nom_rue']
    cityname_col = 'commune'
    citycode_col = 'code_insee'
    postcode_col = None
    keep_debug_cols = True
    id_col = 'numero_dpe'
    addok_search_csv_kwargs = None
    dept_col = None
    simple_geocoding_score_threshold = 0.8

    res, comb = advanced_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                       citycode_col=citycode_col,
                                       postcode_col=postcode_col, keep_debug_cols=True, id_col=id_col,
                                       simple_geocoding_score_threshold=simple_geocoding_score_threshold)

    assert (res.shape[0] == 1000)

    assert (res.query('numero_dpe=="1338L1000113E"')['address_concat'].iloc[
                0] == '86 Avenue Jean Perrot 38100 Grenoble')


def test_failed_geocode_simple_enedis():
    addok_ban_urls['ADDOK_SEARCH_CSV_URL'] = 'your/addok/url'
    addok_ban_urls['ADDOK_SEARCH_CSV_URL'] = 'https://api-adresse.data.gouv.fr/search/csv'
    addok_ban_urls['ADDOK_SEARCH_CSV_URL'] = 'https://api-adresse.data.gouv.fr/search'
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')
    table['code_insee'] = table.CODE_IRIS.apply(lambda x: x[0:5])  # code insee récupéré de l'iris.
    addr_cols_ordered = ['ADRESSE']  # pour les données enedis un seul champs adresse est utilisé(obligatoire)
    cityname_col = 'NOM_COMMUNE'  # le champs du nom de la commune(optionnel : fortement recommandé)
    citycode_col = 'code_insee'  # le champs du code insee(recommandé)
    postcode_col = None  # pas de code postal dans la base enedis(recommandé)
    keep_debug_cols = True  # garde les colonnes intermédiaire de calcul du géocodage (a desactiver pour optimiser mémoire)
    id_col = None  # colonne d'identifiant utilisé dans l'échantillon (sinon par défaut génère un id qui est le numéro de ligne en format str)
    addok_search_csv_kwargs = None  # keyword arguments passés à l'appel à addok_search_csv(geocoding_tools.addok_utils.run_get_addok_search_csv_by_chunks)
    dept_col = None  # colonne renseignant le département
    with pytest.raises(AddokFullFailGeocodeException):
        table_geo_simple = simple_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                                citycode_col=citycode_col,
                                                postcode_col=postcode_col, id_col=id_col,
                                                keep_debug_cols=keep_debug_cols,
                                                )
        assert (table_geo_simple.shape[0] == 1000)


def test_geocode_simple_enedis_nan():
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')
    table['code_insee'] = table.CODE_IRIS.apply(lambda x: x[0:5])  # code insee récupéré de l'iris.
    addr_cols_ordered = ['ADRESSE']  # pour les données enedis un seul champs adresse est utilisé(obligatoire)
    cityname_col = 'NOM_COMMUNE'  # le champs du nom de la commune(optionnel : fortement recommandé)
    citycode_col = 'code_insee'  # le champs du code insee(recommandé)
    postcode_col = None  # pas de code postal dans la base enedis(recommandé)
    keep_debug_cols = True  # garde les colonnes intermédiaire de calcul du géocodage (a desactiver pour optimiser mémoire)
    id_col = None  # colonne d'identifiant utilisé dans l'échantillon (sinon par défaut génère un id qui est le numéro de ligne en format str)
    addok_search_csv_kwargs = None  # keyword arguments passés à l'appel à addok_search_csv(geocoding_tools.addok_utils.run_get_addok_search_csv_by_chunks)
    dept_col = None  # colonne renseignant le département
    s = table.iloc[-1]
    s.ADRESSE = 'sqdnbkqdsjhbdqsijokhdqsjkbnkj'
    s.NOM_COMMUNE = 'sqsssssssssq'
    s.code_insee = '00000'
    table = table.append(s)
    table_geo_simple = simple_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                            citycode_col=citycode_col,
                                            postcode_col=postcode_col, id_col=id_col,
                                            keep_debug_cols=keep_debug_cols,
                                            )
    assert (table_geo_simple.shape[0] == 1001)
    assert (table_geo_simple['address_concat'].isnull().sum()==0)
    assert (table_geo_simple['geocoding_method'].isnull().sum()==0)

def test_geocode_advanced_trusted_codes():
    from pkg_resources import resource_filename
    import pandas as pd
    from geocoding_tools.geocode_table import simple_geocode_table, advanced_geocode_table

    from geocoding_tools.addok_utils import get_addok_search

    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_dle_enedis_2018.csv')
    table = pd.read_csv(file_path, index_col=0, dtype='str')
    table['code_insee'] = table.CODE_IRIS.apply(lambda x: x[0:5])  # code insee récupéré de l'iris.
    table.columns = [el.lower() for el in table.columns]

    addr_cols_ordered = ['adresse']  # pour les données enedis un seul champs adresse est utilisé(obligatoire)
    cityname_col = None  # le champs du nom de la commune(optionnel : fortement recommandé)
    citycode_col = 'code_insee'  # le champs du code insee(recommandé)
    postcode_col = None  # pas de code postal dans la base enedis(recommandé)
    keep_debug_cols = True  # garde les colonnes intermédiaire de calcul du géocodage (a desactiver pour optimiser mémoire)
    id_col = None  # colonne d'identifiant utilisé dans l'échantillon (sinon par défaut génère un id qui est le numéro de ligne en format str)
    addok_search_csv_kwargs = None  # keyword arguments passés à l'appel à addok_search_csv(geocoding_tools.addok_utils.run_get_addok_search_csv_by_chunks)
    dept_col = None  # colonne renseignant le département
    simple_geocoding_score_threshold = 0.8

    from geocoding_tools.assets import dept_table

    table_geo_advanced, t_ = advanced_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col, citycode_col=citycode_col,
                                                    postcode_col=postcode_col, keep_debug_cols=True, id_col=id_col,
                                                    trusted_codes=[citycode_col])

def test_debug_geocode():
    from pkg_resources import resource_filename
    import pandas as pd
    from geocoding_tools.geocode_table import simple_geocode_table, advanced_geocode_table
    from geocoding_tools.scripts_specific_bases.dpe import cleanup_dpe_table
    from geocoding_tools.config import addok_ban_urls
    from geocoding_tools.exceptions import AddokFullFailGeocodeException
    from geocoding_tools.config import debug_config

    debug_config['debug_rejected'] = True
    file_path = resource_filename('geocoding_tools', 'tests/sample_data/sample_td001_dpe_38.csv')

    table = pd.read_csv(file_path, dtype=str, index_col=0).tail(100)
    table = cleanup_dpe_table(table)

    addr_cols_ordered = ['numero_rue', 'type_voie', 'nom_rue']
    cityname_col = 'commune'
    citycode_col = 'code_insee'
    postcode_col = 'code_postal'
    keep_debug_cols = True
    id_col = 'numero_dpe'
    addok_search_csv_kwargs = None
    dept_col = None
    simple_geocoding_score_threshold = 0.8
    table = table.loc[table.numero_dpe=='1338V2000219N']
    res_adv, _ = advanced_geocode_table(table, addr_cols_ordered=addr_cols_ordered, cityname_col=cityname_col,
                                        citycode_col=citycode_col,
                                        postcode_col=postcode_col, keep_debug_cols=True, id_col=id_col,
                                        simple_geocoding_score_threshold=simple_geocoding_score_threshold)
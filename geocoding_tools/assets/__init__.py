import pandas as pd
from pkg_resources import resource_filename

reg_table = pd.read_csv(resource_filename('geocoding_tools', 'assets/reg2016.txt'),
                        encoding='cp1252', sep='\t')

dept_table = pd.read_csv(resource_filename('geocoding_tools', 'assets/depts2016.txt'),
                         encoding='cp1252', sep='\t')

com_insee = pd.read_csv(resource_filename('geocoding_tools', 'assets/communes2020-csv.zip'), dtype='str')

com_cp_poste = pd.read_csv(resource_filename('geocoding_tools', 'assets/2020_10_09_laposte_hexasmal.csv'), dtype='str', sep=';')

com_associe = com_insee.dropna(subset=['comparent']).copy()
com_parent = com_insee.loc[com_insee.com.isin(com_insee.comparent.unique())].copy()
com_parent['comparent'] = com_parent['com']
com_associe = com_associe.append(com_parent)
com_associe = com_associe.drop_duplicates(subset=['com'])
from . import dictionaries

